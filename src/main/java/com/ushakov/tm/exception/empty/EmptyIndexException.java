package com.ushakov.tm.exception.empty;

import com.ushakov.tm.exception.AbstractException;

public class EmptyIndexException extends AbstractException {

    public EmptyIndexException() {
        super("Error! Index is empty!");
    }

}
