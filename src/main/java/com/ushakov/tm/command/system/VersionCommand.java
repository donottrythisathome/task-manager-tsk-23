package com.ushakov.tm.command.system;

import com.ushakov.tm.command.AbstractCommand;
import com.ushakov.tm.exception.empty.EmptyDescriptionException;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.empty.EmptyIndexException;
import com.ushakov.tm.exception.empty.EmptyNameException;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.exception.system.IndexIncorrectException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class VersionCommand extends AbstractCommand {

    @Override
    @Nullable
    public String arg() {
        return "-v";
    }

    @Override
    @Nullable
    public String description() {
        return "Show application version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("2.0.0 \n");
    }

    @Override
    @NotNull
    public String name() {
        return "version";
    }

}
