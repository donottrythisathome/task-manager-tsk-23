package com.ushakov.tm.command.auth;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.exception.entity.UserNotFoundException;
import com.ushakov.tm.model.User;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class UserSetPasswordCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Set user password.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER ID:");
        @NotNull final String userId = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getUserService().findOneById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

    @Override
    @NotNull
    public String name() {
        return "user-set-password";
    }

}
