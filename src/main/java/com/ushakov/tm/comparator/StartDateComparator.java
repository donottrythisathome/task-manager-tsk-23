package com.ushakov.tm.comparator;

import com.ushakov.tm.api.model.IHasDateStart;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

@NoArgsConstructor
public final class StartDateComparator implements Comparator<IHasDateStart> {

    @NotNull
    private static final StartDateComparator INSTANCE = new StartDateComparator();

    @NotNull
    public static StartDateComparator getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasDateStart o1, @Nullable final IHasDateStart o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getDateStart() == null) return 0;
        return o1.getDateStart().compareTo(o2.getDateStart());
    }

}
