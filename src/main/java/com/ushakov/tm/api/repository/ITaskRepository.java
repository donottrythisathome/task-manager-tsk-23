package com.ushakov.tm.api.repository;

import com.ushakov.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IOwnerBusinessRepository<Task> {

    @Nullable
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}